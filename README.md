




wflow workflow


[![star](https://gitee.com/willianfu/jw-workflow-engine/badge/star.svg?theme=dark)](https://gitee.com/willianfu/jw-workflow-engine/ stargazers) [![fork](https://gitee.com/willianfu/jw-workflow-engine/badge/fork.svg?theme=dark)](https://gitee.com/willianfu/jw-workflow- engine/members) 


## 📋 Introduction

`wflow-web` is the front-end designer of the `wflow workflow` project. It is free and open source. This designer includes form design and approval process design.

> Different from the traditional Bpmn's built-in process designer, the traditional designer is obscure and difficult to understand. The threshold for ordinary enterprise users is high, and it is impossible to start without professional training. It requires the assistance of relevant professionals to create processes. The interface of this designer is simple, conforms to the thinking logic of the general public, and is easy to understand and use.

**Note: ** This is the front-end designer part. The supporting workflow back-end has not yet been officially developed. Personal energy is limited and slow. You can continue to pay attention 😅

👩‍👦‍👦Interested students can scan the code to join the exchange group. If the QR code is invalid, you can add me on WeChat `willianfu_` and note `wflow` to pull you into the group




 

Open source is not easy, use love to generate electricity, if you think it is good, you can give a ⭐ star to encourage it

 😋 [github address](https://github.com/willianfu/wflow) | [code cloud gitee](https://gitee.com/willianfu/jw-workflow-engine)

  👉 Online experience address: [Poke me to open the demo page](http://47.100.202.245:83) 👈

  📃 For detailed documentation, please visit: [wflow documentation](https://willianfu.github.io/wflow/)



## 👀 Interface overview

### **Workspace panel**

Enter image description

Enter image description



### Form management

 **Workflow form management, support grouping and single-group form drag-and-drop sorting**

Enter image description

Enter image description


---------

#### **Form Basic Settings**

Enter image description




--------

#### **Form Designer**

> Support column layout, detailed table, and various basic components, support custom development components

![image-20220724220114724](https://pic.rmb.bdstatic.com/bjh/b0f1ed22d61ea86b4222b89dbea6ecd1.png)

![image-20220724221040780](https://pic.rmb.bdstatic.com/bjh/73e71e1323812a57802a76beffe27906.png)






---------

 #### Process Designer

> Arbitrary condition level approval process design, approval node supports multiple business type settings, supports process verification

![image-20220711111351476](https://pic.rmb.bdstatic.com/bjh/3300dbc60218a8376b45ed6ed46e8162.png)



**Custom approval conditions**

![image-20220722182318650](https://pic.rmb.bdstatic.com/bjh/4599e414142004f3b0445e478018b8be.png)


---------

**Custom complex flow conditions**

> Visualize process logic branch conditions

![image-20220722182622661](https://pic.rmb.bdstatic.com/bjh/299989bb8b256beae152a29ea611b790.png)

---------



 **Support multiple types of business nodes, support configuration verification, flexible configuration**

image-20220722182427315

**Supports unlimited levels of nesting**

![image-20220711111911427](https://pic.rmb.bdstatic.com/bjh/02cd8936e081bdd053bfa695826817ba.png)

**Auto-check setting items, list all error prompts**

image-20220731215022817

**Conditional node priority is dynamically dragged and refreshed in real time**

Enter image description



## ✍Development



📃 For detailed documentation, please visit: [wflow documentation](https://willianfu.github.io/wflow/)



### Project Structure

````
├─api interface
├─assets
│ └─image
├─components Generic components
│ └─common
├─router routing
├─store vuex, designer data store
├─utils
└─views main page and view
    ├─admin
    │ └─layout
    │ ├─form Form Design
    │ └─process design
    ├─common
    │ ├─form
    │ │ ├─components form components
    │ │ ├─config Form component configuration
    │ │ ├─expand extension
    │ │ └─settings
    │ └─process
    │ ├─config Process node settings
    │ └─nodes process node
    └─workspace
````



### Designer data

> The designer's data is stored in Vuex. When it needs to be passed to the backend, it is directly taken out and submitted to the interface

````json
{
    "formId":"37289743892", //Form ID, generated by the backend
    "formName":"Replenishment card application", //Form name
    "logo":{ //icon
        "icon":"icon/image base64",
        "background":"#FEFEFE" //If it is a picture, it will not take effect
    },
    //Form permissions and other attribute settings
    "settings":{
        "commiter": [], //Who can submit the form
    	"admin":[], //Form administrator, can edit and export data
        "singn": false, // Whether a signature is required for global setting approval
        "notify":{
            "type": "APP", //Notification type APP QQ WX DING EMAIL
            "title": "Message notification title"
        }
    },
    "group":20, //in the group
    "formItems":[],//Form design data
    "process":{}, //process data
    "remark":"Remark Description"
}
````



## 💗 Reward List

Thanks to the following friends for their support, and to everyone who paid attention to this project and made valuable suggestions.

| Nickname (in chronological order) | Amount |
| ------------------------------------------------- ------------- | ---- |
| * base | 9.9 |
| * Learning Dragon | 30 |
| * Fai | 50 |
| vincert | 50 |
| . | 30 |



## 💪 Support the author

**Open source is not easy and needs encouragement. If you think this project is helpful, please invite me for a cup of coffee😋**
Authors are supported